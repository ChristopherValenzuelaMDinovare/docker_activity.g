import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  name = 'Christopher Valenzuela'
  title = 'MARVEL HEROES';
  Ironman = 'Iron Man: Hello'
  Blessie = 'TopeHandsome: Hi'

  items = ['item1', 'item2', 'item3'];
  //The addItem() method takes an argument in the form of a string and then adds that string to the items array.
  addItem(newItem: string) {
    this.items.push(newItem);
  }

}