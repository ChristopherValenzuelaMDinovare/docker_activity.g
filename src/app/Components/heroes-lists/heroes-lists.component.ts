import { Component } from '@angular/core';
import { Heroes } from 'src/app/Interfaces/heroes';
import { HEROES } from '../Mock-data/mock-heroes';
@Component({
  selector: 'app-heroes-lists',
  templateUrl: './heroes-lists.component.html',
  styleUrls: ['./heroes-lists.component.css']
})
export class HeroesListsComponent {
  heroes = HEROES;
  selectedHero?: Heroes;
  heroName: string = '';
  

  onSelect(hero: Heroes): void {
    this.selectedHero = hero;
    this.heroName = hero.name;
    this.selectedHero.id = hero.id;
  }



  
  hero: Heroes = {
    id: 1,
    name: 'Iron Man'
    
  };
}
